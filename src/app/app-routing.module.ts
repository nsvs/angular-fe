import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth-guard.service';
import { NoAuthGuard } from './core/guards/no-auth-guard.service';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
    data: {
      state: 'login',
    },
    canActivate: [NoAuthGuard]
  },
  {
    path: 'encode',
    loadChildren: './encode/encode.module#EncodeModule',
    data: {
      state: 'encode',
    },
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/encode'
  },
  {
    path: '**',
    redirectTo: '/encode'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
