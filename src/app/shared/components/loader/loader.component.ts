import { Component, Input, HostBinding } from '@angular/core';

@Component({
  selector: '[app-loader]',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
  host: {
    class: 'text-center'
  }
})
export class LoaderComponent {
  @Input('visible') visible = true;

  get visiblity() {
    return !this.visible;
  }
}
