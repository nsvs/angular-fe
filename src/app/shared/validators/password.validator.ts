import { AbstractControl } from '@angular/forms';


export function PasswordValidator(control: AbstractControl) {
    const { value } = control
    if (!isPasswordValid(value))
        return { invalidPassword: true }

    return null;
}

function isPasswordValid(input) {
    return input.length > 5 && input.split('').some(isNumeric);
}

function isNumeric(char) {
    return !isNaN(parseFloat(char)) && isFinite(char);
}