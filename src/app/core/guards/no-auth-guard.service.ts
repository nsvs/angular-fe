import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate() {
    const { token } = this.authService;

    if (!token) return true;

    this.router.navigate(['/']);

    return false;
  }
}
