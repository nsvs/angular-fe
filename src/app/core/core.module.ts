import { EncodeService } from './services/encode.service';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './guards/auth-guard.service';
import { AuthInterceptor, ErrorInterceptor } from './interceptors';
import { ApiService, AuthService, CacheService } from './services';
import { NoAuthGuard } from './guards/no-auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [

        // services
        ApiService,
        AuthService,
        EncodeService,
        CacheService,

        // guards
        AuthGuard,
        NoAuthGuard,

        // interceptors
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true
        },
      ]
    }
  }
}
