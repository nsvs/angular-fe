import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

@Injectable()
export class CacheService {
  private cacheKey = 'angular-app';

  private get cache() {
    return JSON.parse(localStorage.getItem(this.cacheKey));
  }

  setToCache({ key, value }): Observable<any> {
    let cache = Object.freeze(this.cache);

    cache = {
      ...cache,
      [key]: value
    };

    localStorage.setItem(this.cacheKey, JSON.stringify(cache));

    return of(this.cache);
  }

  clearFromCache(key: string): Observable<any> {
    let cache = this.cache;

    delete cache[key];

    localStorage.setItem(this.cacheKey, JSON.stringify(cache));

    return of(this.cache);
  }

  getCache(key: string) {
    return this.cache && this.cache[key];
  }
}
