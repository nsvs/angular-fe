import { IEncodeQuery, IEncodeResponseSingle } from './../../../models/api.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '.';

@Injectable()
export class EncodeService {
  url = '/encode'

  constructor(private apiService: ApiService) { }

  getEncodedString(encodeQuery?: IEncodeQuery): Observable<IEncodeResponseSingle> {
    const valueToEncode = encodeQuery && this.apiService.mapParams(encodeQuery);
    return this.apiService.get(this.url, valueToEncode);
  }

}