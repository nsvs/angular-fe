import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  mapParams(obj: any): HttpParams {
    let params: HttpParams = new HttpParams();

    Object.keys(obj).forEach(key => {
      const paramValue = obj[key];

      if (
        paramValue === null
        || paramValue === undefined
        || !String(paramValue).length
      ) {
        return;
      }

      params = params.append(key, obj[key])
    });

    return params;
  }

  get(path: string, params?: HttpParams): Observable<any> { 
    return this.httpClient.get(`${environment.apiUrl}${path}`, { params });
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.httpClient.post(`${environment.apiUrl}${path}`, body);
  }

  postFormData(path: string, body: Object = {}): Observable<any> {
    const headers = new HttpHeaders();
    const formData = new FormData();

    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    Object.keys(body).forEach(key => formData.set(key, body[key]));

    return this.httpClient.post(`${environment.apiUrl}${path}`, formData);
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.httpClient.put(`${environment.apiUrl}${path}`, body);
  }

  putFormData(path: string, body: Object = {}): Observable<any> {
    const headers = new HttpHeaders();
    const formData = new FormData();

    headers.append('Content-Type', 'application/x-www-form-urlencoded');

    Object.keys(body).forEach(key => formData.set(key, body[key]));

    return this.httpClient.put(`${environment.apiUrl}${path}`, formData);
  }

  delete(path: string): Observable<any> {
    return this.httpClient.delete(`${environment.apiUrl}${path}`);
  }
}
