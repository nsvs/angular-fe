import { Injectable } from '@angular/core';
import { Observable, zip } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ApiService } from './api.service';
import { CacheService } from './cache.service';
import { CacheKeys } from '../../../models';

@Injectable()
export class AuthService {
  private cacheKeys = CacheKeys;

  constructor(
    private apiService: ApiService,
    private cacheService: CacheService
  ) { }

  login({ email, password }): Observable<any> {
    return this.apiService.post('/login', { email, password }).pipe(
      switchMap(({ token }) => {

        return zip(
          this.cacheService.setToCache({ key: this.cacheKeys.token, value: token }),
        );
      })
    );
  }

  logout(): Observable<any> {
    return zip(
      this.cacheService.clearFromCache(this.cacheKeys.token),
    );
  }

  get token(): string {
    return this.cacheService.getCache(this.cacheKeys.token);
  }

}

