import { IEncodeQuery } from './../../../models/api.model';
import { EncodeService } from './../../core/services/encode.service';
import { Component, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: '[app-encode]',
  templateUrl: './encode.component.html',
  styleUrls: ['./encode.component.css']
})
export class EncodeComponent {
  @ViewChild('encodeForm', { static: false }) form: FormGroup;
  isLoaderVisible: boolean;
  encodedValue: string;

  constructor(
    private encodeService: EncodeService
  ) { }

  getEncondedString({ stringToEncode }) {
    this.isLoaderVisible = true;

    const encodeQuery: IEncodeQuery = {
      value: stringToEncode
    }

    this.encodeService.getEncodedString(encodeQuery).subscribe(({value}) => {
      this.encodedValue = value;
      this.isLoaderVisible = false;
    }, e => {
      this.isLoaderVisible = false;
    });
  }

}
