import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { PasswordValidator } from 'src/app/shared/validators/password.validator';

@Component({
  selector: '[app-login]',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup;
  loginError: string;
  isLoaderVisible: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, PasswordValidator]],
    });
  }

  submit() {
    const { email, password } = this.loginForm.value
    this.loginError = null;
    this.isLoaderVisible = true;

    this.authService.login({ email, password })
      .subscribe(_ => {
        this.router.navigate(['/encode']);
      }, _ => {
        this.loginError = 'Invalid credentials'
        this.isLoaderVisible = false;
      });
  }

}
