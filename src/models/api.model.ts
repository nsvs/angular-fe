export interface IEncodeQuery {
  value?: string; 
}

export interface IEncodeResponseSingle {
  value?: string;
}
